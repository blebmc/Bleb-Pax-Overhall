gamerule sendCommandFeedback false

execute if entity @s[scores={charge_count=1..}] run playsound minecraft:entity.blaze.shoot player @a ~ ~ ~ 1 1.5
execute if entity @s[scores={charge_count=1..}] run playsound minecraft:entity.generic.burn player @a ~ ~ ~ 0.2 1

execute if entity @s[scores={charge_count=1..}] run execute anchored eyes run summon fireball ^ ^-0.1 ^1.2 {Tags:["rotationMotion"],ExplosionPower:3,direction:[0.0,0.0,0.0]}

execute if entity @s[scores={charge_count=1..}] run clear @s minecraft:fire_charge{fireorb:1} 1

tag @s add Cast