execute as @a[tag=Cast] run gamerule sendCommandFeedback true
execute as @a[tag=Cast] run scoreboard players set @s range 120

tag @a[tag=Cast] remove Cast

execute as @a[scores={carrots=1..},nbt={SelectedItem:{tag:{magma:1}}}] at @s run function carrots:fireball/fireball
execute as @a[scores={carrots=1..},nbt={SelectedItem:{tag:{health:1}}}] at @s run function carrots:health/health

execute as @a[scores={carrots=1..},nbt={SelectedItem:{tag:{hex:1}}}] at @s anchored eyes run function debug:hex

scoreboard players set @a carrots 0
