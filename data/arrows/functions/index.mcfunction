execute if entity @s[type=arrow] store result score %list customArrows run data get entity @s CustomPotionEffects[0].Amplifier
execute if entity @s[type=!arrow] store result score %list customArrows run data get entity @s ActiveEffects[{Id:27b}].Amplifier

execute if score %list customArrows matches 21 run function arrows:tnt
execute if score %list customArrows matches 22 run function arrows:blaze
execute if score %list customArrows matches 23 run function arrows:lightning
execute if score %list customArrows matches 24 run function arrows:poison
execute if score %list customArrows matches 25 run function arrows:web

kill @s[type=arrow]